package org.eclipse.nation;

import org.springframework.stereotype.Component;

@Component
public interface European {

	public void saluer();
}
